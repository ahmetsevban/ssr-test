import { Component } from '@angular/core';

@Component({
  selector: 'ssr-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ssr-test-app';
}
